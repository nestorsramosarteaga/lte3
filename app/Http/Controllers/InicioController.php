<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InicioController extends Controller
{
    //
    public function index(){
      return view('inicio',['layout' => 'fixedNavbar']);
    }

    //
    public function layoutBoxed(){
      return view('inicio',['layout' => 'boxed']);
    }

    public function layoutFixedNavbar(){
      return view('inicio',['layout' => 'fixedNavbar']);
    }

    public function layoutTopNavbar(){
      return view('inicio',['layout' => 'topNavbar']);
    }
}
