<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LTE 3 | Top Navigation Layout</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href='{{asset("assets/$theme/plugins/fontawesome-free/css/all.min.css")}}'>
    <!-- Ionicons
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    -->
    <!-- Theme style -->
    <link rel="stylesheet" href='{{asset("assets/$theme/dist/css/adminlte.min.css")}}'>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body class="hold-transition layout-top-nav">
    <!-- Site wrapper -->
    <div class="wrapper">
      <!-- inicio header -->
      @include("themes/$theme/$layout/header")
      <!-- fin header -->

      <!-- inicio content -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <div class="container-fluid">
          </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="container">
            <div class="row">

              <div class="col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">Card title</h5>

                    <p class="card-text">
                      Some quick example text to build on the card title and make up the bulk of the card's
                      content.
                    </p>

                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                  </div>
                </div>

                <div class="card card-primary card-outline">
                  <div class="card-body">
                    <h5 class="card-title">Card title</h5>

                    <p class="card-text">
                      Some quick example text to build on the card title and make up the bulk of the card's
                      content.
                    </p>
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                  </div>
                </div><!-- /.card -->
              </div>
              <!-- /.col-md-6 -->
              
              <div class="col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title m-0">Featured</h5>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Special title treatment</h6>

                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                  </div>
                </div>

                <div class="card card-primary card-outline">
                  <div class="card-header">
                    <h5 class="card-title m-0">Featured</h5>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Special title treatment</h6>

                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                  </div>
                </div>
              </div>
              <!-- /.col-md-6 -->

            </div>
            <!-- /.row -->
          </div>
          <!-- /.container -->
        </section>
        <!-- /.content -->

      </div>
      <!-- fin content -->
      <!-- inicio footer -->
      @include("themes/$theme/footer")
      <!-- fin footer -->
    </div>

    <!-- jQuery -->
    <script src='{{asset("assets/$theme/plugins/jquery/jquery.min.js")}}'></script>
    <!-- Bootstrap 4 -->
    <script src='{{asset("assets/$theme/plugins/bootstrap/js/bootstrap.bundle.min.js")}}'></script>
    <!-- AdminLTE App -->
    <script src='{{asset("assets/$theme/dist/js/adminlte.min.js")}}'></script>
    <!-- AdminLTE for demo purposes -->
    <script src='{{asset("assets/$theme/dist/js/demo.js")}}'></script>
  </body>
</html>
