<?php
Route::get('/', 'InicioController@index');
Route::get('/boxed', 'InicioController@layoutBoxed');
Route::get('/fixed-navbar', 'InicioController@layoutFixedNavbar');
Route::get('/top-navbar', 'InicioController@layoutTopNavbar');
